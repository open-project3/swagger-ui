/* eslint-disable import/extensions */
import express from 'express';
import swaggerUI from 'swagger-ui-express';
import bodyParser from 'body-parser';
import { swaggerDef } from './config/config.js'; // Import as a named export
import api from './routes/api.js';

const app = express();
const port = 3001;

app
  .use('/swagger-ui', swaggerUI.serve, swaggerUI.setup(swaggerDef))
  .use(bodyParser.json())
  .use(api);
app.listen(port, () => {
  console.log(`Server is running on port ${port}`);
});
