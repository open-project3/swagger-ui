import swaggerJSDoc from 'swagger-jsdoc';

const swaggerDef = swaggerJSDoc({
  swaggerDefinition: {
    openapi: '3.0.0',
    info: {
      title: 'Swagger Express JS',
      version: '1.0.0',
      description: 'Swagger Documentation Demo on Express JS'
    }
  },
  apis: ['src/**/*.js']
});

const apiRoute = Object.freeze({
  apiUrl: 'http://demo9930637.mockable.io'
});

export { swaggerDef, apiRoute }; // Export as named exports
