/**
 * @swagger
 *  tags:
 *   - name: Swagger
 *     description: Everything about swaggers
 *   - name: Books
 *     description: API documentation about books
 * /about-swagger/:
 *   get:
 *     summary: About swagger
 *     description: This is a swagger
 *     tags:
 *       [Swagger]
 *     responses:
 *       200:
 *         description: SUCCESS
 *       500:
 *         description: INTERNAL ERROR
 * /books:
 *   get:
 *     summary: All book list
 *     description: Get all books list
 *     tags:
 *       [Books]
 *     responses:
 *       200:
 *         description: The list of the books
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 id:
 *                    type: integer
 *                    description: id.
 *                    example: 1
 *                 title:
 *                    type: string
 *                    description: title
 *                    example: The Pragmatic Programmer
 *                 author:
 *                    type: string
 *                    description: author
 *                    example: Andy Hunt / Dave Thomas
 *                 finished:
 *                    type: boolean
 *                    description: finished
 *                    example: false
 *                 createdAt:
 *                    type: date
 *                    description: created at
 *                    example: 2023-10-10T02:03:34.369Z
 *       500:
 *         description: INTERNAL ERROR
 *   post:
 *     summary: Create a new book
 *     tags:
 *       [Books]
 *     parameters:
 *      - in : body
 *        type: object
 *        properties:
 *            title:
 *               type: string
 *            author:
 *               type: string
 *            finished:
 *               type: boolean
 *        example:
 *          title: Avengers 2
 *          author: Ben James
 *          finished: true
 *     responses:
 *       204:
 *         description: The created book.
 *       500:
 *         description: Some server error
 *
 * /books/{id}:
 *   get:
 *     summary: Get the book by id
 *     tags: [Books]
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: integer
 *         required: true
 *         description: The book id
 *     responses:
 *       200:
 *         description: The list of the books
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 id:
 *                    type: integer
 *                    description: id.
 *                    example: 1
 *                 title:
 *                    type: string
 *                    description: title
 *                    example: The Pragmatic Programmer
 *                 author:
 *                    type: string
 *                    description: author
 *                    example: Andy Hunt / Dave Thomas
 *                 finished:
 *                    type: boolean
 *                    description: finished
 *                    example: false
 *                 createdAt:
 *                    type: date
 *                    description: created at
 *                    example: 2023-10-10T02:03:34.369Z
 *   put:
 *    summary: Update the book by the id
 *    tags: [Books]
 *    parameters:
 *      - in: path
 *        name: id
 *        schema:
 *          type: string
 *        required: true
 *        description: The book id
 *    requestBody:
 *      required: true
 *      content:
 *        application/json:
 *          schema:
 *            type: object
 *            properties:
 *              title:
 *                type: string
 *              author:
 *                type: string
 *              finished:
 *                type: boolean
 *            example:
 *              title: Avengers 2
 *              author: Ben James
 *              finished: true
 *    responses:
 *      204:
 *        description: The book was updated
 *      404:
 *        description: The book was not found
 *      500:
 *        description: Some error happened
 *   delete:
 *     summary: Remove the book by id
 *     tags: [Books]
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *         required: true
 *         description: The book id
 *     responses:
 *       204:
 *         description: The book was deleted
 *       404:
 *         description: The book was not found
 */
