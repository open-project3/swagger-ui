/* eslint-disable import/extensions */
import express from 'express';
import axios from 'axios';
import books from '../util/data.js';
import { apiRoute } from '../config/config.js';

const app = express();
const router = express.Router();
const ABOUT_SWAGGER = '/about-swagger';
const BOOK = '/books';
const BOOK_BY_ID = '/books/:id';

app.get(ABOUT_SWAGGER, async (req, res) => {
  try {
    const response = await axios.get(`${apiRoute.apiUrl}${ABOUT_SWAGGER}`);
    res.json(response.data);
  } catch (error) {
    res.status(500).json({ error: 'Internal Server Error' });
  }
});

router.get(BOOK, (req, res) => {
  res.status(200).json(books);
});

router.post(BOOK, (req, res) => {
  const { title, author, finished } = req.body;

  const book = {
    id: books.length + 1,
    title,
    author,
    finished: finished !== undefined ? finished : false,
    createdAt: new Date()
  };

  books.push(book);

  res.status(200).json(book);
});

router.get(BOOK_BY_ID, (req, res) => {
  const book = books.find((item) => {
    return +item.id === +req.params.id;
  });
  if (book !== null) {
    res.status(200).json(book);
  }
  res.sendStatus(404);
});

function validateUpdateData(book, title, author, finished) {
  const updated = {
    id: book.id,
    title: title !== undefined ? title : book.title,
    author: author !== undefined ? author : book.author,
    finished: finished !== undefined ? finished : book.finished,
    createdAt: book.createdAt
  };
  return updated;
}

router.put(BOOK_BY_ID, (req, res) => {
  const book = books.find((item) => {
    return +item.id === +req.params.id;
  });
  if (book) {
    const { title, author, finished } = req.body;
    const updated = validateUpdateData(book, title, author, finished);
    books.splice(books.indexOf(book), 1, updated);
    res.status(200).json({ before: book, after: updated });
  }
  res.sendStatus(404);
});

router.delete(BOOK_BY_ID, (req, res) => {
  const book = books.find((item) => {
    return +item.id === +req.params.id;
  });

  if (book) {
    const updated = books.splice(books.indexOf(book), 1);
    res.status(200).json({ before: book, after: updated });
  }
  return res.sendStatus(404);
});

export default router;
